/**
 * Created by sahid on 3/22/16.
 */



jQuery(function(){

    $(".navbar-menu li a[href*=#],.footer_nav_menu li a[href*=#]").click(function(event){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 1000,'easeInQuart');
        event.preventDefault();
    });

    $(".close").click(function(e){

        $(this).parents(".modal_main").css("margin-top","-800px").hide();
        $(this).parents(".modal_shadow").fadeOut();
    })
    $(".show_popup").click(function(e){
        e.preventDefault();
        $(".modal_shadow").fadeIn(0, function(){

            $(".modal_main").show().stop().animate({
                'margin-top':50+'px',
            },100)
            //$(".modal_main").css("margin-top","50px").slideDown('fast');
        });

    })

})



jQuery(window).load(function() {


    /*
    Home page animation after page load
     */
    //$(".site_title span").hide();
    var textLine = $(".tagline span");
    var leftElements = $(".left_banner .animate_part span");
    var rightElement = $(".right_banner .animate_part span");
    var tl = new TimelineLite();

    setTimeout(function(){
        jQuery(".load").fadeOut(500,function(){

            tl.staggerFrom(textLine, 0.8, {top:"-=30px", rotation:"-40deg", alpha:0, scale:1.8, ease:Back.easeOut}, 0.5);
            TweenLite.set($(".site_title"), {css:{visibility:"visible"}});

            tl.staggerFrom('.left_banner , .right_banner',0.2,{scale:0,rotate:180, autoAlpha:0},0)
                .addLabel('targetPoint')
                .staggerFrom(leftElements,1, {opacity:0,x:-200,ease:Back.easeOut},0.5,'targetPoint')
                .staggerFrom(rightElement,1, {opacity:0,x:200,ease:Back.easeOut},0.5,'targetPoint');

            TweenLite.set($(".left_banner, .right_banner"), {css:{visibility:"visible"}});



            /*
             Landing Page Animation on page load
             */

            var landing_page_tl = new TimelineLite();

            landing_page_tl.from('#land_section_1 .time',.4,{opacity:0, autoAlpha:0},0)
                .from("#land_section_1 .land_content",0.5,{x:200,opacity:0})
                .from("#land_section_1 .land_content p",.5,{opacity:0, autoAlpha:0},.8);

            if($(window).width()<768) {

                landing_page_tl.from('#land_section_2 .time', .4, {opacity: 0, autoAlpha: 0,delay:1}, .2)
                    .addLabel('landing_time')
                    .from("#land_section_2 .land_content", 0.5, {x: 200, opacity: 0,delay:1}, .4, 'landing_time')
                    .from("#land_section_2 .land_content_2", 0.5, {x: 200, opacity: 0,delay:1}, .4, 'landing_time')
                    .addLabel('content_time')
                    .from("#land_section_2 .land_content p", .5, {opacity: 0, autoAlpha: 0,delay:1}, .8, 'content_time')
                    .from("#land_section_2 .land_content_2 p", .5, {opacity: 0, autoAlpha: 0,delay:1}, .8, 'content_time')
                    .staggerFrom("#land_section_2 .playstore a", 0.5, {scale: 0, rotate: 180, autoAlpha: 0,delay:.3}, .2);

                    TweenLite.set($("#land_section_2 .box"), {css:{visibility:"visible"}});

            }

            TweenLite.set($("#land_section_1 .box"), {css:{visibility:"visible"}});

        });



        //If page load by other page with #ID location
        //Then load the page with animation
        if (location.hash){
            setTimeout(function(){
                $('html, body').scrollTop(0).show(200);
                jump();
            }, 0);
        }

    },1000);




    });

jQuery(function(){
    var controller = new ScrollMagic.Controller();

        var text  = "<p>Smart Campus会員になることで </p>" +
                    "<p> すべてのサービスをひとつのアカウントで一括して </p> " +
                    "<p> ストレスなくスムーズに使えるようになりました。</p>" +
                    "<p> 対象サービスのアプリ内のCampus worksで</p> " +
                    "<p> アンケートに協力したり、イベントスタッフとして働くことで </p>" +
                    "<p> 発生するCポイントで人気のアイテムや </p>" +
                    "<p> 電子マネーと交換することもできます。</p> <br>" +
                    "<p> 今しかできないことに全力で取り組めるように </p>" +
                    "<p> Smart Campusは大学生をもっと自由にします。</p>";


    var sec_1_tl = new TimelineLite();
    var sec_2_tl = new TimelineLite();
    var sec_3_tl = new TimelineLite();
    var sec_4_tl = new TimelineLite();
    var sec_5_tl = new TimelineLite();

    var sec_1_logo = sec_1_tl.staggerFrom('.main_logo, .para_one_lp',1.3,{opacity:0, autoAlpha:0},0.2)

    var sec_2 = sec_2_tl.staggerFrom('#section_2 ',0.5,{y:"200", opacity:0},0.3)
                        .staggerFrom('.t_1 .title',0.5,{x:"-200",opacity:0},0)
                        .staggerFrom('.t_1 .inner_item ',0.2,{y:"100", opacity:0},0.1);


    var sec_3 = sec_3_tl.staggerFrom('.t_2 .title',0.5,{x:"-200",opacity:0},0)
                        .staggerFrom('.t_2 .inner_item ',0.2,{y:"100", opacity:0},0.3);

    var sec_3_1 =sec_5_tl.staggerFrom('.t_3 .title',0.5,{x:"-200",opacity:0},0)
                        .staggerFrom('.t_3 .inner_item ',0.2,{y:"100", opacity:0},0.2);

    var sec_4 = sec_4_tl.staggerFrom('.t_4 .title',0.5,{x:"-200",opacity:0},0)
                        .staggerFrom('.t_4 .inner_item ',0.2,{y:"100", opacity:0},0.2)



    var scene = new ScrollMagic.Scene({triggerElement: "#section_1", reverse:false })
        .setTween(sec_1_logo)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller)
        .on('start end', function(){
            $(".para_two_lp").typed({
                strings: [text],
                showCursor: false,
                typeSpeed:.5,
                backDelay: 100
            });
        });

    //For mobile view change the trigger point
        var first_sec = '.para_two_lp';

       if($(window).width()<768){
           first_sec = '.section_1_text';
       }

    var scene = new ScrollMagic.Scene({triggerElement: first_sec,reverse:false })
        .setTween(sec_2)
        //.addIndicators({name:'t_1'}) // add indicators (requires plugin)
        .addTo(controller);

    var scene = new ScrollMagic.Scene({triggerElement: '.t_1', reverse:false})
        .setTween(sec_3)
        //.addIndicators({name:'t_2'}) // add indicators (requires plugin)
        .addTo(controller);
    var scene = new ScrollMagic.Scene({triggerElement: '.t_2', reverse:false})
        .setTween(sec_3_1)
        //.addIndicators({name:'t_2'}) // add indicators (requires plugin)
        .addTo(controller);
    var scene = new ScrollMagic.Scene({triggerElement: '.t_3',  reverse:false})
        .setTween(sec_4)
        //.addIndicators({name:"t_3"}) // add indicators (requires plugin)
        .addTo(controller);

    TweenLite.set($(".t_1, .t_2, .t_3, .t_4"), {css:{visibility:"visible"}});

    //scene.destroy(true);



    //create animate for landing page
    var landing_sec_1 = new TimelineLite();
    var landing_sec_2 = new TimelineLite();
    var landing_sec_3 = new TimelineLite();
    var landing_sec_4 = new TimelineLite();
    var landing_sec_5 = new TimelineLite();
    var landing_sec_6 = new TimelineLite();

    //Section 2
    if($(window).width()>769) {
        var land_sec_2 = landing_sec_1.from('#land_section_2 .time', .4, {opacity: 0, autoAlpha: 0}, .2)
            .addLabel('landing_time')
            .from("#land_section_2 .land_content", 0.5, {x: 200, opacity: 0}, .4, 'landing_time')
            .from("#land_section_2 .land_content_2", 0.5, {x: 200, opacity: 0}, .4, 'landing_time')
            .addLabel('content_time')
            .from("#land_section_2 .land_content p", .5, {opacity: 0, autoAlpha: 0}, .8, 'content_time')
            .from("#land_section_2 .land_content_2 p", .5, {opacity: 0, autoAlpha: 0}, .8, 'content_time')
            .staggerFrom("#land_section_2 .playstore a", 0.5, {scale: 0, rotate: 180, autoAlpha: 0}, .2);
    }

    //Section 3
    var land_sec_3 = landing_sec_2.from('#land_section_3 .time',.1,{opacity:0, autoAlpha:0},0)
                    .from("#land_section_3 .land_content",0.5,{x:200,opacity:0})
                    .from("#land_section_3 .land_content p",.5,{opacity:0, autoAlpha:0},.8);

    //Section 4
    var land_sec_4 = landing_sec_3.from('#land_section_4 .time',.1,{opacity:0, autoAlpha:0},0)
        .addLabel('landing_time')
        .from("#land_section_4 .land_content",0.5,{x:200,opacity:0},.4,'landing_time')
        .from("#land_section_4 .land_content_2",0.5,{x:200,opacity:0},.4,'landing_time')
        .addLabel('content_time')
        .from("#land_section_4 .land_content p",.8,{opacity:0, autoAlpha:0},'content_time')
        .from("#land_section_4 .land_content_2 p",.2,{opacity:0, autoAlpha:0},'content_time')
        .staggerFrom("#land_section_4 .playstore a",0.5,{scale:0,rotate:180, autoAlpha:0},.2);

    //Section 5
    var land_sec_5 = landing_sec_4.from('#land_section_5 .time',.1,{opacity:0, autoAlpha:0},0)
        .from("#land_section_5 .land_content",0.5,{x:200,opacity:0})
        .from("#land_section_5 .land_content p",.5,{opacity:0, autoAlpha:0},.8);

//Section 4
    var land_sec_6 = landing_sec_5.from('#land_section_6 .time',.1,{opacity:0, autoAlpha:0},0)
        .addLabel('landing_time')
        .from("#land_section_6 .land_content",0.5,{x:200,opacity:0},.4,'landing_time')
        .from("#land_section_6 .land_content_2",0.5,{x:200,opacity:0},.4,'landing_time')
        .addLabel('content_time')
        .from("#land_section_6 .land_content p",.5,{opacity:0, autoAlpha:0},'content_time')
        .from("#land_section_6 .land_content_2 p",.5,{opacity:0, autoAlpha:0},'content_time')
        .staggerFrom("#land_section_6 .playstore a",0.5,{scale:0,rotate:180, autoAlpha:0},.2);
    //Section 7
    var land_sec_7 = landing_sec_6.from('#land_section_7 .time',.1,{opacity:0, autoAlpha:0},0)
        .from("#land_section_7 .land_content",0.5,{x:200,opacity:0})
        .from("#land_section_7 .land_content p",.5,{opacity:0, autoAlpha:0},.8);


    //Section 1 Magic
    if($(window).width()>769) {
        var scene = new ScrollMagic.Scene({triggerElement: '#land_section_2', reverse: false})
            .setTween(land_sec_2)
            //.addIndicators({name:'t_1'}) // add indicators (requires plugin)
            .addTo(controller);
        TweenLite.set($("#land_section_2 .box"), {css:{visibility:"visible"}});
    }

    //Section 2 Magic
    var scene = new ScrollMagic.Scene({triggerElement: '#land_section_3',reverse:false })
        .setTween(land_sec_3)
        //.addIndicators({name:'t_2'}) // add indicators (requires plugin)
        .addTo(controller);
    TweenLite.set($("#land_section_3 .box"), {css:{visibility:"visible"}});

    //Section 3 Magic
    var scene = new ScrollMagic.Scene({triggerElement: '#land_section_4',reverse:false })
        .setTween(land_sec_4)
        //.addIndicators({name:'t_3'}) // add indicators (requires plugin)
        .addTo(controller);
    TweenLite.set($("#land_section_4 .box"), {css:{visibility:"visible"}});

    //Section 4 Magic
    var scene = new ScrollMagic.Scene({triggerElement: '#land_section_5',reverse:false })
        .setTween(land_sec_5)
        //.addIndicators({name:'t_4'}) // add indicators (requires plugin)
        .addTo(controller);
    TweenLite.set($("#land_section_5 .box"), {css:{visibility:"visible"}});

    //Section 5 Magic
    var scene = new ScrollMagic.Scene({triggerElement: '#land_section_6',reverse:false })
        .setTween(land_sec_6)
        //.addIndicators({name:'t_5'}) // add indicators (requires plugin)
        .addTo(controller);
    TweenLite.set($("#land_section_6 .box"), {css:{visibility:"visible"}});

//Section 6 Magic
    var scene = new ScrollMagic.Scene({triggerElement: '#land_section_7',reverse:false })
        .setTween(land_sec_7)
        //.addIndicators({name:'t_7'}) // add indicators (requires plugin)
        .addTo(controller);
    TweenLite.set($("#land_section_7 .box"), {css:{visibility:"visible"}});
})



//Catch the others page link
//Tern to animate the target location with animate
var jump=function(e)
{
    if (e){
        e.preventDefault();
        var target = $(this).attr("href");
    }else{
        var target = location.hash;
    }

    $('html,body').animate(
        {
            scrollTop: $(target).offset().top
        },1000,function()
        {
            location.hash = target;
        });

}

$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
  });

  $(window).scroll(function(){
  if ($(this).scrollTop() > 200) {
    $('.scroll_top').fadeIn();
  } else {
    $('.scroll_top').fadeOut();
  }
});

$(function(){
       if (screen.width >= 640) {
            $('input:radio').screwDefaultButtons({
                   image: 'url("images/radio_icon.png")',
                   width: 31,
                   height: 30
           });
       } else {
           $('input:radio').screwDefaultButtons({
                   image: 'url("images/radio_icon_sp_18.png")',
                   width: 18,
                   height: 17.5
           });
       }
   });
